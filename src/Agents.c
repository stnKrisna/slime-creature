#include "Agents.h"
#include <math.h>
#include <stdlib.h>
#include "World.h"
#include "random.h"
#include "AgentsThreadData.h"

#define NUM_THREADS 8
#define SENSOR_SIZE 3
#define SENSOR_SPACING_ANGLE 45

void Agent_init (Agents * agent, AgentIndexT agentCount) {
    agent->agentCount = agentCount;
    
    agent->agentColors = (SDL_Color *)malloc(sizeof(SDL_Color) * agentCount);
    agent->agentTransforms = (Transform *)malloc(sizeof(Transform) * agentCount);
    
    agent->agentVelocity = 0.1;
    agent->agentRadius = 2;
    agent->turnSpeed = 0.02;
    agent->agentSensorOffset = 1;
}

void Agent_free (Agents * agent) {
    free(agent->agentColors);
    free(agent->agentTransforms);
    
    agent->agentColors = NULL;
    agent->agentTransforms = NULL;
    
    agent->agentCount = 0;
}

/// Get sensor reading from an agent. agentIndex is the index of agent of which to retreive the readings from, and sensorAngleRelative is the sensor angle (in radians) relative to the agent
float Agent_Sense (Engine * engine, World * world, AgentIndexT agentIndex, float sensorAngleRelative, float sensorOffsetFromAgent) {
    Transform * t = &world->agents.agentTransforms[agentIndex];
    
    // The sensor angle relative to the global coordinate
    float sensorAngleGlobal = t->rotation + sensorAngleRelative;
    
    Vector2 sensorDirection = {cos(sensorAngleGlobal), sin(sensorAngleGlobal)};
    Vector2 sensorGlobalPosition = {
        t->position.x + (sensorDirection.x * sensorOffsetFromAgent),
        t->position.y + (sensorDirection.y * sensorOffsetFromAgent)
    };
    float areaReading = 0;
    
    // Raw sensor reading
    uint8_t red, green, blue;
    
    for (int sensorOffsetY = -SENSOR_SIZE; sensorOffsetY < SENSOR_SIZE; ++sensorOffsetY) {
        for (int sensorOffsetX = -SENSOR_SIZE; sensorOffsetX < SENSOR_SIZE; ++sensorOffsetX) {
            Vector2 sensorReadFrom = {
                roundf(sensorGlobalPosition.x + sensorOffsetX),
                roundf(sensorGlobalPosition.y + sensorOffsetY),
            };
            
            if (sensorReadFrom.x >= 0 && sensorReadFrom.x < world->width && sensorReadFrom.y >= 0 && sensorReadFrom.y < world->height) {
                Engine_getPixel(engine, (int)sensorReadFrom.x, (int)sensorReadFrom.y, &red, &green, &blue);
                
                areaReading += (float)(red + green + blue) / 3.0f;
            }
        }
    }
    
    return areaReading;
}

/// Draw circle pixel-by-pixel
void DrawCircle (Engine * engine, int32_t centreX, int32_t centreY, int32_t radius) {
    SDL_Color renderColor;
    SDL_GetRenderDrawColor(engine->renderer, &renderColor.r, &renderColor.g, &renderColor.b, NULL);
    
    int diameter = radius * 2;
    for (int w = 0; w < diameter; w++) {
        for (int h = 0; h < diameter; h++) {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx*dx + dy*dy) <= (radius * radius)) {
                Engine_setPixel(engine, centreX + dx, centreY + dy, renderColor.r, renderColor.g, renderColor.b);
            }
        }
    }
}

int Agent_updateThread (void * data) {
    AgentUpdateThreadData * updateData = (AgentUpdateThreadData *)data;
    
    World * world = updateData->world;
    Engine * engine = updateData->engine;
    Vector2 direction = {0, 0};
    Vector2 newPosition = {0, 0};
    Transform * entityTransform;
    float agentTurnSpeed = world->agents.turnSpeed;
    
    for (AgentIndexT i = 0; i < updateData->entityCount; ++i) {
        entityTransform = &updateData->entityTransform[i];
        
        // Calculate new agent position
        direction.x = cos(entityTransform->rotation);
        direction.y = sin(entityTransform->rotation);
        
        newPosition.x = entityTransform->position.x + (direction.x * world->agents.agentVelocity);
        newPosition.y = entityTransform->position.y + (direction.y * world->agents.agentVelocity);
        
        // Clamp position
        if (newPosition.x <= 0 || newPosition.x >= world->width || newPosition.y <= 0 || newPosition.y >= world->height) {
            newPosition.x = fmin(world->width - 0.01, fmax(0, newPosition.x));
            newPosition.y = fmin(world->height - 0.01, fmax(0, newPosition.y));
            
            entityTransform->rotation = getRandomAngle();
        }
        
        entityTransform->position.x = newPosition.x;
        entityTransform->position.y = newPosition.y;
        
        // Read sensor data and turn towards the direction with the strongest scent
        float sensorForwardWeight = Agent_Sense(engine, world, updateData->entityStartIndex + i, 0, world->agents.agentSensorOffset);
        float sensorLeftWeight = Agent_Sense(engine, world, updateData->entityStartIndex + i, SENSOR_SPACING_ANGLE, world->agents.agentSensorOffset);
        float sensorRightWeight = Agent_Sense(engine, world, updateData->entityStartIndex + i, -SENSOR_SPACING_ANGLE, world->agents.agentSensorOffset);
        float randomTurnAngle = getRandomFloat();
        
        // Turn left
        if (sensorLeftWeight > sensorRightWeight)
            entityTransform->rotation += randomTurnAngle * 2 * agentTurnSpeed;

        // Turn right
        else if (sensorLeftWeight < sensorRightWeight)
            entityTransform->rotation -= randomTurnAngle * 2 * agentTurnSpeed;
        
        // Random turning
        else if (sensorForwardWeight < sensorLeftWeight & sensorForwardWeight < sensorRightWeight)
            entityTransform->rotation += (randomTurnAngle - 0.5) * 2 * agentTurnSpeed;
        
        // No need to check for going the same direction because the default behaviour is to go in the same direction
    }
    
    return 0;
}

void Agent_update (Engine * engine, void * params) {
    World * world = (World *)params;
    SDL_Thread ** threads = (SDL_Thread **)malloc(sizeof(SDL_Thread *) * NUM_THREADS);
    int * threadsReturns = (int *)malloc(sizeof(int) * NUM_THREADS);
    AgentUpdateThreadData * datas = (AgentUpdateThreadData *)malloc(sizeof(AgentUpdateThreadData) * NUM_THREADS);
    
    AgentIndexT agentCountPerThread = world->agents.agentCount / NUM_THREADS;
    AgentIndexT agentStartIndex = 0;
    
    // Spawn thread
    for (unsigned int i = 0; i < NUM_THREADS; ++i) {
        if (i < NUM_THREADS - 1) {
            // Divide all agents equally across all workers
            datas[i].entityCount = agentCountPerThread;
        } else {
            // In case we have some leftover agents, assign everything else to the last worker
            datas[i].entityCount = world->agents.agentCount - agentStartIndex;
        }
        
        datas[i].entityTransform = &world->agents.agentTransforms[agentStartIndex];
        datas[i].world = world;
        datas[i].engine = engine;
        datas[i].entityStartIndex = agentStartIndex;
        
        agentStartIndex += agentCountPerThread;
        
        threads[i] = SDL_CreateThread(Agent_updateThread, "AgentUpdates", (void *)(&datas[i]));
    }
    
    // Wait for thread
    for (unsigned int i = 0; i < NUM_THREADS; ++i) {
        if (threads[i] == NULL) {
            printf("SDL_CreateThread failed: %s\n", SDL_GetError());
        } else {
            SDL_WaitThread(threads[i], &(threadsReturns[i]));
        }
    }
    
    free(threads);
    free(threadsReturns);
    free(datas);
}

/// Get an agent's transform using its index
Transform * getAgentTransform (Agents * agents, AgentIndexT index) {
    return &agents->agentTransforms[index];
}

void Agent_draw (Engine * engine, void * params) {
    World * world = (World *)params;
    Agents * agents = &world->agents;
    Transform * t = NULL;
    SDL_Color * renderColor = NULL;
    
    for (AgentIndexT i = 0; i < agents->agentCount; ++i) {
        t = getAgentTransform(agents, i);
        renderColor = &agents->agentColors[i];
        
        Engine_setPixel(engine, t->position.x, t->position.y, renderColor->r, renderColor->g, renderColor->b);
    }
}
