#pragma once

#include <SDL.h>
#include "Engine.h"
#include <stdint.h>

typedef struct Vector2 {
    float x;
    float y;
} Vector2;

/// Entity transform
typedef struct Transform {
    /// The position of the entity
    Vector2 position;
    
    /// Rotation of the agent (in radians)
    float rotation;
} Transform;

/// Agent index typedef
typedef uint32_t AgentIndexT;

/// Collection of agents
typedef struct Agents {
    /// Collection of agent transforms
    Transform * agentTransforms;
    
    /// Collection of agent colors
    SDL_Color * agentColors;
    
    /// How many agent information is stored here
    AgentIndexT agentCount;

    float agentVelocity;
    int agentRadius;
    float turnSpeed;
    float agentSensorOffset;
} Agents;

/// Initialize the agent struct
void Agent_init (Agents * agent, unsigned int agentCount);

/// Free the agent struct. You are responsible on freeing the agent struct after calling this function
void Agent_free (Agents * agent);

/// Agent update action
void Agent_update (Engine * engine, void * params);

/// Agent draw action
void Agent_draw (Engine * engine, void * params);
