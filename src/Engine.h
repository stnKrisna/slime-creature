#pragma once

#include <stdbool.h>
#include <SDL.h>
#include <stdint.h>
#include <limits.h>

/// Int type for a pixel
typedef uint32_t PixelT;

typedef struct Engine {
    void (* onUpdate)(struct Engine * engine, void * params);
    void (* onDraw)(struct Engine * engine, void * params);
    
    PixelT * frameBuffer;
    PixelT * blurBuffer;
    
    PixelT windowWidth;
    PixelT windowHeight;
    
    bool performReload;
    char configFile[PATH_MAX];
    
    bool isRunning;
    double deltaTime;
    
    SDL_Window * window;
    SDL_Surface * screenSurface;
    SDL_Texture * renderSurface;
    SDL_Renderer * renderer;
} Engine;

/// Initialize an engine. Make sure window & screenSurface is set to NULL
int Engine_init (Engine * engine, PixelT w, PixelT h);

/// Create a new window
SDL_Window * Engine_makeWindow (Engine * engine, PixelT w, PixelT h);

/// Start the main loop
void Engine_start (Engine * engine, void * params);

/// Perform internal cleanup. You are responsible to cleanup the structure of the enigne
void Engine_exit (Engine * engine);

/// Handle events
void Engine_eventHandler (Engine * engine, SDL_Event * e);

/// Get pixel value at a specific location
void Engine_getPixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue);

/// Get pixel value at a specific location
static inline void Engine_getPixel (Engine * engine, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue) {
    Engine_getPixelBuffer(engine, engine->frameBuffer, x, y, red, green, blue);
}

/// Get average pixel color on a specific location
void Engine_getAveragePixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue);

/// Get average pixel color on a specific location
static inline void Engine_getAveragePixel (Engine * engine, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue) {
    Engine_getAveragePixelBuffer(engine, engine->frameBuffer, x, y, red, green, blue);
}

/// Set pixel color
void Engine_setPixel (Engine * engine, int x, int y, uint8_t red, uint8_t green, uint8_t blue);

/// Set pixel color
void Engine_setPixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t red, uint8_t green, uint8_t blue);
