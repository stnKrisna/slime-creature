#pragma once

#include <SDL.h>
#include "Engine.h"
#include "Agents.h"

typedef float LerpT;
typedef float LerpCacheT;

typedef struct World {
    PixelT width;
    PixelT height;
    Agents agents;
    float trailLifeSpan;
    float opacityAccumulator;
    float trailDiffuseSpeed;
    
    LerpCacheT * lerpCache;
} World;

/// Initialize a World struct
void World_init (World * world, PixelT w, PixelT h, unsigned int agentCount, float spawnRadius);

/// Free a world object
void World_free (World * world);

/// World object update action
void World_update (Engine * engine, void * params);

/// World object draw action
void World_draw (Engine * engine, void * params);
