#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <limits.h>

#include "Engine.h"
#include "World.h"
#include <math.h>

#include "ini/ini.h"

#define WORLD_WIDTH 800
#define WORLD_HEIGHT 600
#define ENTITY_COUNT 50000
#define AGENT_VELOCITY 3.5
#define AGENT_SPAWN_RADIUS fmin(simParam.worldWidth,simParam.worldHeight)/2
#define AGENT_TURN_SPEED 0.5
#define AGENT_SENSOR_OFFSET 40
#define TRAIL_LIFE_SPAN 60
#define TRAIL_DIFFUSE_SPEED 0.91

typedef struct SimulationParameter {
    // World config
    uint32_t worldWidth;
    uint32_t worldHeight;
    AgentIndexT entityCount;
    int32_t trailLifeSpan;
    float trailDiffuseSpeed;
    
    // Agent config
    float agentVelocity;
    int32_t agentSpawnRadius;
    float agentTurnSpeed;
    float agentSensorOffset;
    
    // Misc
    uint32_t seed;
} SimulationParameter;

/// Simulation config loader handler
static int simConfigHandler (void* cfg, const char* section, const char* name, const char* value) {
    SimulationParameter * simParam = (SimulationParameter *)cfg;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    
    // Agent control
    if (MATCH("agentControl", "agentVelocity")) {
        simParam->agentVelocity = atof(value);
    } else if (MATCH("agentControl", "agentSpawnRadius")) {
        simParam->agentSpawnRadius = atoi(value);
    } else if (MATCH("agentControl", "agentTurnSpeed")) {
        simParam->agentTurnSpeed = atof(value);
    } else if (MATCH("agentControl", "agentSensorOffset")) {
        simParam->agentSensorOffset = atof(value);
    }
    
    // World config
    else if (MATCH("worldConfig", "worldWidth")) {
        simParam->worldWidth = atoi(value);
    } else if (MATCH("worldConfig", "worldHeight")) {
        simParam->worldHeight = atoi(value);
    } else if (MATCH("worldConfig", "entityCount")) {
        simParam->entityCount = atoi(value);
    } else if (MATCH("worldConfig", "trailLifeSpan")) {
        simParam->trailLifeSpan = atoi(value);
    } else if (MATCH("worldConfig", "trailDiffuseSpeed")) {
        simParam->trailDiffuseSpeed = atof(value);
    }
    
    // Misc
    else if (MATCH("misc", "seed")) {
       simParam->seed = atoi(value);
    }
    
    // Catch all
    else {
        return 0;  /* unknown section/name, error */
    }

    return 1;
}

int engineRun (char * configFile) {
    SimulationParameter simParam;
    
    // Load initial value for the sim params
    simParam.worldWidth = WORLD_WIDTH;
    simParam.worldHeight = WORLD_HEIGHT;
    simParam.entityCount = ENTITY_COUNT;
    simParam.agentVelocity = AGENT_VELOCITY;
    simParam.agentSpawnRadius = AGENT_SPAWN_RADIUS;
    simParam.agentTurnSpeed = AGENT_TURN_SPEED;
    simParam.agentSensorOffset = AGENT_SENSOR_OFFSET;
    simParam.trailLifeSpan = TRAIL_LIFE_SPAN;
    simParam.trailDiffuseSpeed = TRAIL_DIFFUSE_SPEED;
    simParam.seed = time(NULL);
    
    // Load simulation config
    if (strlen(configFile) > 0) {
        if (ini_parse(configFile, simConfigHandler, &simParam) < 0) {
            printf("Can't load 'simParams.ini'\n");
            return 1;
        }
    }
    
    // Init PRNG
    srand(simParam.seed);
    
    // Create new engine
    Engine engine;
    
    // Create new world
    World world;
    
    // Init the world
    World_init(&world, simParam.worldWidth, simParam.worldHeight, simParam.entityCount, simParam.agentSpawnRadius);
    
    // Simulation parameters
    world.agents.turnSpeed = simParam.agentTurnSpeed;
    world.trailLifeSpan = simParam.trailLifeSpan;
    world.agents.agentSensorOffset = simParam.agentSensorOffset;
    world.trailDiffuseSpeed = simParam.trailDiffuseSpeed;
    
    // Set the agent velocity
    world.agents.agentVelocity = simParam.agentVelocity;
    
    // Init the engine
    Engine_init(&engine, simParam.worldWidth, simParam.worldHeight);
    
    // Register update & draw
    engine.onUpdate = &World_update;
    engine.onDraw = &World_draw;
    
    // Run the engine's loop
    Engine_start(&engine, (void *)(&world));
    
    // Free world resources
    World_free(&world);
    
    // Copy requested config file
    strcpy(configFile, engine.configFile);
    bool engineReload = engine.performReload;
    
    // Cleanup engin
    Engine_exit(&engine);
    
    return engineReload;
}

int main (int argc, char ** argv) {
    char configFile[PATH_MAX];
    configFile[0] = '\0';
    
    while (engineRun(configFile)) {}
    
    return 0;
}
