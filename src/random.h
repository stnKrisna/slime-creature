#pragma once

/// Get a random float between 0 and 1
float getRandomFloat ();

/// Get a random angle
float getRandomAngle ();
