#include "World.h"
#include "stdio.h"
#include <stdlib.h>
#include "random.h"
#include <math.h>
#include <limits.h>

void randomPointInCirlce (float * x, float * y, float radius) {
    float randomRadius = getRandomFloat() * radius;
    float randomAngle = getRandomFloat() * 2 * M_PI;
    
    (*x) = randomRadius * cos(randomAngle);
    (*y) = randomRadius * sin(randomAngle);
}

void World_init (World * world, PixelT w, PixelT h, unsigned int agentCount, float spawnRadius) {
    world->width = w;
    world->height = h;
    
    // Spawn the entity
    Agent_init(&world->agents, agentCount);
    
    SDL_Color * entityColor;
    Transform * entityTransform;
    for (AgentIndexT i = 0; i < agentCount; ++i) {
        entityColor = &world->agents.agentColors[i];
        entityTransform = &world->agents.agentTransforms[i];
        
        // Pick a random point in a circle
        randomPointInCirlce(&entityTransform->position.x, &entityTransform->position.y, spawnRadius);
        
        // Move spawn point relative to the center of the world
        entityTransform->position.x += world->width / 2;
        entityTransform->position.y += world->height / 2;
        
        // Give the entity a random direction
        entityTransform->rotation = getRandomAngle();
        
        // Set default color
        entityColor->r = entityColor->g = entityColor->b = entityColor->a = 255;
    }
    
    // Set initial value for the world
    world->opacityAccumulator = 0;
    world->trailLifeSpan = 1;
    world->trailDiffuseSpeed = 0.9;
    
    // Init lerp cache. Lerp cache size must be UCHAR_MAX^2 for value of a and value of b
    uint32_t lerpCacheSize = sizeof(LerpCacheT) * UCHAR_MAX * UCHAR_MAX;
    world->lerpCache = (LerpCacheT *)malloc(lerpCacheSize);
    memset(world->lerpCache, 0, lerpCacheSize);
}

void World_free (World * world) {
    Agent_free(&world->agents);
    free(world->lerpCache);
}

void World_update (Engine * engine, void * params) {
    World * world = (World *)params;
    Agent_update(engine, params);
}

/// Calculate the alpha based on the target life span of the trail
int World_TrailLifeSpanOpacityCalculator (World * world, float deltaTime) {
    float trailOpacity = fmin(255.0f, 255.0f / (world->trailLifeSpan * deltaTime));
    
    bool accumulatorAboveMinimum = world->opacityAccumulator >= 1;
    bool trailOpacityAboveMinimum = trailOpacity >= 1;
    bool aggregateOpacityAboveMinimum = accumulatorAboveMinimum | trailOpacityAboveMinimum;
    
    if (!aggregateOpacityAboveMinimum) {
        world->opacityAccumulator += trailOpacity;
        trailOpacity = 0;
    } else if (aggregateOpacityAboveMinimum) {
        trailOpacity += world->opacityAccumulator;
        world->opacityAccumulator = 0;
    }
    
    return (int)trailOpacity;
}

LerpT lerp (uint8_t a, uint8_t b, float x) {
    return (float)(a + (b -a )) * x;
}

LerpT lerp_withCache (World * world, uint8_t a, uint8_t b, float x) {
    LerpCacheT * cacheVal = &world->lerpCache[(UCHAR_MAX * b) + a];
    
    if ((*cacheVal) == 0)
        (*cacheVal) = lerp(a, b, x);
    
    return (*cacheVal);
}

void swap (uint32_t ** a, uint32_t ** b) {
    uint32_t * tmp = *a;
    *a = *b;
    *b = tmp;
}

void World_updateTrailOpacityAndDiffusion (Engine * engine, World * world) {
    uint8_t original_r, original_g, original_b;
    uint8_t blur_r, blur_g, blur_b;
    float diffusion_r, diffusion_g, diffusion_b;
    uint8_t total_r, total_g, total_b;
    
    int reducedOpacity = World_TrailLifeSpanOpacityCalculator(world, engine->deltaTime);
    
    // Diffuse trails
    for (PixelT y = 0; y < engine->windowHeight; y++) {
        for (PixelT x = 0; x < engine->windowWidth; x++) {
            Engine_getPixelBuffer(engine, engine->frameBuffer, x, y, &original_r, &original_g, &original_b);
            Engine_getAveragePixelBuffer(engine, engine->frameBuffer, x, y, &blur_r, &blur_g, &blur_b);
            
            if (original_r == blur_r & original_g == blur_g & original_b == blur_b)
                continue;

            diffusion_r = lerp_withCache(world, original_r, blur_r, world->trailDiffuseSpeed);
            diffusion_g = lerp_withCache(world, original_g, blur_g, world->trailDiffuseSpeed);
            diffusion_b = lerp_withCache(world, original_b, blur_b, world->trailDiffuseSpeed);

            total_r = (uint8_t)fmax(0, diffusion_r - reducedOpacity);
            total_g = (uint8_t)fmax(0, diffusion_g - reducedOpacity);
            total_b = (uint8_t)fmax(0, diffusion_b - reducedOpacity);

            Engine_setPixelBuffer(engine, engine->blurBuffer, x, y, total_r, total_g, total_b);
        }
    }
}

void World_draw (Engine * engine, void * params) {
    SDL_SetRenderDrawColor(engine->renderer, 0, 0, 0, 255);
    SDL_RenderClear(engine->renderer);
    
    World * world = (World *)params;
    
    // Draw the agents
    Agent_draw(engine, params);
    
    // Lower trail TTL
    World_updateTrailOpacityAndDiffusion(engine, world);
    
    SDL_UpdateTexture(engine->renderSurface, NULL, engine->frameBuffer, engine->windowWidth * sizeof(uint32_t));
    
    swap(&engine->frameBuffer, &engine->blurBuffer);
}
