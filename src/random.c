#include "random.h"
#include <stdlib.h>
#include <math.h>

float getRandomFloat () {
    return (float)(rand() % 10000) / 10000.0f;
}

float getRandomAngle () {
    return getRandomFloat() * 2 * M_PI;
}
