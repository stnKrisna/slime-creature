#pragma once

#include "Agents.h"
#include "World.h"

/// Update action thread data structure
typedef struct AgentUpdateThreadData {
    World * world;
    AgentIndexT entityCount;
    Transform * entityTransform;
    Engine * engine;
    AgentIndexT entityStartIndex;
} AgentUpdateThreadData;
