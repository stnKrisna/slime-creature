#include "Engine.h"
#include <stdint.h>
#include "appSignature.h"
#include <string.h>

#define WINDOW_TITLE_BUFFER 64

int Engine_init (Engine * engine, PixelT w, PixelT h) {
    uint32_t subsystem_init = SDL_WasInit(SDL_INIT_EVERYTHING);
    
    // Only init SDL if we've not initialized it yet
    if (SDL_WasInit(SDL_INIT_EVERYTHING) == 0) {
        // Initialize video
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
            SDL_Log("Unable to initialize SDL: %s\n", SDL_GetError());
            return 1;
        }
    }
    
    if (engine->window == NULL) {
        // Create the window here
        engine->window = Engine_makeWindow(engine, w, h);
        
        // Make sure window was successfully created
        if( engine->window == NULL ) {
            SDL_Log( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            return 1;
        }
        
        // Get the window surface here
        engine->screenSurface = SDL_GetWindowSurface(engine->window);
    }
    
    // Make sure to set the engine callback actions to NULL
    engine->onDraw = NULL;
    engine->onUpdate = NULL;
    
    // Set the renderer
    engine->renderer = SDL_GetRenderer(engine->window);
    
    // Create a new render texture
    engine->renderSurface = SDL_CreateTexture(engine->renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, w, h);
    
    engine->windowWidth = w;
    engine->windowHeight = h;
    
    const PixelT bufferAllocationSize = sizeof(PixelT) * w * h;
    
    engine->frameBuffer = (PixelT *)malloc(bufferAllocationSize);
    engine->blurBuffer = (PixelT *)malloc(bufferAllocationSize);
    
    memset(engine->frameBuffer, 0, bufferAllocationSize);
    memset(engine->blurBuffer, 0, bufferAllocationSize);
    
    // Init the delta time
    engine->deltaTime = 0;
    
    engine->performReload = false;
    
    return 0;
}

SDL_Window * Engine_makeWindow (Engine * engine, PixelT w, PixelT h) {
    return SDL_CreateWindow( APP_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, SDL_WINDOW_SHOWN );
}

void Engine_start (Engine * engine, void * params) {
    engine->isRunning = true;
    SDL_Event e;
    uint64_t now = SDL_GetPerformanceCounter();
    uint64_t last = 0;
    
    char defaultWindowTitle[WINDOW_TITLE_BUFFER];
    strcpy(defaultWindowTitle, SDL_GetWindowTitle(engine->window));
    
    char customWindowTitle[WINDOW_TITLE_BUFFER];
    
    // Clear render texture
    SDL_Texture * defaultTexture = SDL_GetRenderTarget(engine->renderer);
    SDL_SetRenderDrawColor(engine->renderer, 0, 0, 0, 255);
    SDL_RenderClear(engine->renderer);
    
    float deltaTimeAccumulator = 999;
    
    while (engine->isRunning && !engine->performReload) {
        // Update window title
        if (deltaTimeAccumulator > 500) {
            snprintf(customWindowTitle, WINDOW_TITLE_BUFFER, "%s (%.0f FPS)", defaultWindowTitle, floor(1000/engine->deltaTime));
            SDL_SetWindowTitle(engine->window, customWindowTitle);
            deltaTimeAccumulator = 0;
        } else {
            deltaTimeAccumulator += engine->deltaTime;
        }
        
        // Update delta time
        last = now;
        now = SDL_GetPerformanceCounter();
        engine->deltaTime = (double)((now - last) * 1000 / (double)SDL_GetPerformanceFrequency() );
        
        // Handle event
        Engine_eventHandler(engine, &e);
        
        // Perform update
        if (engine->onUpdate != NULL)
            engine->onUpdate(engine, params);
        
        // Perform draw
        if (engine->onDraw != NULL) {
            SDL_SetRenderTarget(engine->renderer, engine->renderSurface);
            engine->onDraw(engine, params);
            SDL_SetRenderTarget(engine->renderer, NULL);
        }
        
        // Copy render texture
        SDL_RenderCopy(engine->renderer, engine->renderSurface, NULL, NULL);
        
        // Display
        SDL_RenderPresent( engine->renderer );
        
        if (engine->deltaTime < 1000/30.0f) {
            SDL_Delay((1000/30.0f) - engine->deltaTime);
        }
    }
}

void Engine_exit (Engine * engine) {
    SDL_DestroyWindow( engine->window );
    SDL_DestroyTexture( engine->renderSurface );
    
    engine->window = NULL;
    engine->renderSurface = NULL;
    
    free(engine->frameBuffer);
    free(engine->blurBuffer);
    
    engine->blurBuffer = engine->frameBuffer = NULL;
    
    memset(engine->configFile, 0, PATH_MAX);
    
    SDL_Quit();
}

/// On exit event handler
void onExit (Engine * engine, SDL_Event * e) {
    engine->isRunning = false;
}

/// On dropped file event handler
void onDropFile (Engine * engine, SDL_Event * e) {
    // Copy the file name to the engine config parameter and reload the application
    
    strcpy(engine->configFile, e->drop.file);
    engine->performReload = true;
}

void Engine_eventHandler (Engine * engine, SDL_Event * e) {
    while (SDL_PollEvent( e ) != 0) {
        //User requests quit
        if( e->type == SDL_QUIT )
            onExit(engine, e);
        else if ( e->type == SDL_DROPFILE )
            onDropFile(engine, e);
    }
}

void Engine_setPixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t red, uint8_t green, uint8_t blue) {
    if (x < 0 || x >= engine->windowWidth || y < 0 || y >= engine->windowHeight)
        return;
    
    uint32_t color = 0;
    
    color += red;
    color <<= 8;
    color += green;
    color <<= 8;
    color += blue;
    
    buffer[(y * engine->windowWidth) + x] = color;
}

void Engine_setPixel (Engine * engine, int x, int y, uint8_t red, uint8_t green, uint8_t blue) {
    Engine_setPixelBuffer(engine, engine->frameBuffer, x, y, red, green, blue);
}

void Engine_getPixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue) {
    uint32_t pixel = buffer[(y * engine->windowWidth) + x];
    
    (*red) = (pixel & 0xFF0000) >> 16;
    (*green) = (pixel & 0x00FF00) >> 8;
    (*blue) = (pixel & 0x0000FF) >> 0;
}

void Engine_getAveragePixelBuffer (Engine * engine, Uint32 * buffer, int x, int y, uint8_t * red, uint8_t * green, uint8_t * blue) {
    int red_total = 0,
        green_total = 0,
        blue_total = 0;

    // Totals the color values of the input pixel (x, y) and the 8 pixels that surround it.
    for(int row = -1; row <= 1; ++row) {
        int current_y = y + row;
        for(int col = -1; col <= 1; ++col) {
            int current_x = x + col;

            // Only grab values of pixels that are actually within the window
            if(current_x >= 0 && current_x < engine->windowWidth &&
               current_y >= 0 && current_y < engine->windowHeight) {

                // Get the color value of the current pixel
                Uint32 color = buffer[current_x + (current_y * engine->windowWidth)];

                // Bit shift RGB values of pixel color into color totals.
                red_total += (uint8_t)((color & 0xFF0000) >> 16);
                green_total += (uint8_t)((color & 0x00FF00) >> 8);
                blue_total += (uint8_t)((color & 0x0000FF));
            }
        }
    }
    
    (*red) = (uint8_t)(red_total/9);
    (*green) = (uint8_t)(green_total/9);
    (*blue) = (uint8_t)(blue_total/9);
}
